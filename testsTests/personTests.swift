//
//  testsTests.swift
//  testsTests
//
//  Created by Pereiro, Delfin on 29/06/2018.
//  Copyright © 2018 Pereiro, Delfin. All rights reserved.
//

import XCTest
@testable import tests

class personTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit() {
        let name = "Peter"
        let age = 54
        let person = Person(name: name, age: age)
        XCTAssertEqual(person.name, name)
       XCTAssertEqual(person.age, age)
    }
}
