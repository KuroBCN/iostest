//
//  NewPersonViewController.swift
//  tests
//
//  Created by Pereiro, Delfin on 29/06/2018.
//  Copyright © 2018 Pereiro, Delfin. All rights reserved.
//

import UIKit

class NewPersonViewController: UIViewController {

    var parentList: MasterViewController?
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onDoneBtnClick(_ sender: Any) {
        createEntry(with: nameTextField.text, and: ageTextField.text)
        self.dismiss(animated: true, completion: nil)
    }
    
    func createEntry(with name: String?, and age: String?){
        if let name = name, let ageString = age, let age = Int(ageString) {
            let newPerson = Person(name: name, age: age)
            parentList?.persons.append(newPerson)
        }
    }
}
