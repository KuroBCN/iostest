//
//  Person.swift
//  tests
//
//  Created by Pereiro, Delfin on 29/06/2018.
//  Copyright © 2018 Pereiro, Delfin. All rights reserved.
//

import Foundation
class Person {
    let name : String
    let age : Int
    
    init(name : String, age : Int) {
        self.name = name
        self.age = age
    }
    
    func speak() -> String {
        return "blablabla..."
    }
}
