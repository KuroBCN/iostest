//
//  DetailViewController.swift
//  tests
//
//  Created by Pereiro, Delfin on 29/06/2018.
//  Copyright © 2018 Pereiro, Delfin. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    
    var person: Person?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.        
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = person {
            detailDescriptionLabel.text = "\(detail.name) has \(detail.age) years"
        }
    }
}

